import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from 'src/app/Services/app.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styleUrls: ['./inicio-sesion.component.css']
})
export class InicioSesionComponent implements OnInit {

  //estados: [];
  //odelBusqueda: Iestado = {} as any;
  loginForm :FormGroup;
  response: Observable<HttpResponse<Object>>;
  responsev: HttpResponse<Object>;
  variable: String = "*";
  auth: string;

  constructor(private router:Router,
    private formBuilder: FormBuilder, private appService:AppService) { }

  ngOnInit() {
    //this.modelBusqueda = {} as any;
    this.loginForm = this.formBuilder.group
    ({
      usuario: [],
      password: []
    })
  }

    

  onSubmit()
  {
    this.appService.validatelogin(this.loginForm.value)
    .pipe()
    .subscribe(
      (data: HttpResponse<any>) => {
        console.log(data.headers.get('henry.ruiz'));
        this.auth = data.headers.get('henry.ruiz').toString().replace('Bearer ','');
        console.log(this.auth);
        localStorage.removeItem('auth');
        localStorage.setItem('auth', this.auth);
        this.router.navigate(['gestion-personas']);
      });
  }
  

}
