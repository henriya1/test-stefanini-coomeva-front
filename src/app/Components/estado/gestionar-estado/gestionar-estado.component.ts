import { Component, OnInit } from '@angular/core';
import { Iestado } from 'src/app/Interfaces/iestado';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AgregarEstadoComponent } from '../agregar-estado/agregar-estado.component';
import { EstadoService } from 'src/app/Services/estado.service';

@Component({
  selector: 'app-gestionar-estado',
  templateUrl: './gestionar-estado.component.html',
  styleUrls: ['./gestionar-estado.component.css']
})
export class GestionarEstadoComponent implements OnInit {

  estados: Iestado[];
  modelBusqueda: Iestado = {} as any;
  searchForm :FormGroup;
  auth: string;

  constructor(private router:Router,
    private formBuilder: FormBuilder, private estadoService:EstadoService) { }

  ngOnInit() {
    this.auth =  localStorage.getItem('auth');
    this.modelBusqueda = {} as any;
    this.gestionarEstado(this.modelBusqueda);
    this.searchForm = this.formBuilder.group
    ({
      CODIGO: []
    })
  }

  gestionarEstado(modelB){

    this.estadoService.getAllEstado(modelB,this.auth).subscribe(
      (data: Iestado[]) => this.estados = data
      ,
      (error) => {      
          console.error("Ha ocurrido un error, contacte con el administrador");
      }
    );

  }

  agregarEstado(): void
  {
    this.router.navigate(['agregar-estado']);
  }

  editarEstado(estado: Iestado)
  {
    localStorage.removeItem('codigoEstado');
    localStorage.setItem('codigoEstado', estado.codigo.toString());
    this.router.navigate(['editar-estado']);
  }


  eliminarEstado(estado: Iestado)
  {
    this.modelBusqueda = {} as any;
    this.modelBusqueda.codigo = estado.codigo;
    this.estadoService.deleteEstadoId(this.modelBusqueda.codigo,this.auth).subscribe(
      (data: String) => {
      alert('El registro fue eliminado con exito');
      this.modelBusqueda = {} as any;
      this.gestionarEstado(this.modelBusqueda);
      }
    )
  }

  onSubmit() {
    
    if (this.searchForm.value.CODIGO) {
      this.estados = [];
      this.modelBusqueda = this.searchForm.value;
      this.estadoService.getIdEstado(this.searchForm.get('CODIGO').value,this.auth).subscribe(
        (data: Iestado) => this.estados[0] = data
        ,
        (error) => {      
            console.error("Ha ocurrido un error, contacte con el administrador");
        }
      );
    }
    else {
      this.modelBusqueda = {} as any;
      this.gestionarEstado(this.modelBusqueda);
    }
  }

}
