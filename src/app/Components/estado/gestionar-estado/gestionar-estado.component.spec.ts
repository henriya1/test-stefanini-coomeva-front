import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionarEstadoComponent } from './gestionar-estado.component';

describe('GestionarEstadoComponent', () => {
  let component: GestionarEstadoComponent;
  let fixture: ComponentFixture<GestionarEstadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionarEstadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionarEstadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
