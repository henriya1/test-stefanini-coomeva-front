import { Component, OnInit } from '@angular/core';
import { Iestado } from 'src/app/Interfaces/iestado';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EstadoService } from 'src/app/Services/estado.service';

@Component({
  selector: 'app-editar-estado',
  templateUrl: './editar-estado.component.html',
  styleUrls: ['./editar-estado.component.css']
})
export class EditarEstadoComponent implements OnInit {

  modelEstado : Iestado = {} as any;
  estados: Iestado[] = [];
  auth: string;
  editForm = this.formBuilder.group(
    {
      codigo: ['', Validators.required ],
      nombre: [],
      fechaCreacion: [],
      usuarioCreacion: [],
      fechaModificacion: [],
      usuarioModificacion: []
    }
  );

  constructor(private formBuilder: FormBuilder, private router: Router, private estadoService:EstadoService) { }

  ngOnInit() {

    this.editForm = this.formBuilder.group(
      {
        codigo: ['', Validators.required ],
        nombre: [],
        fechaCreacion: [],
        usuarioCreacion: [],
        fechaModificacion: [],
        usuarioModificacion: []
      }
    );

    let codigoEstado =  localStorage.getItem('codigoEstado');
    console.log("ID COMPONENTE EDIT ANTES" + codigoEstado);
    if (!codigoEstado) {
      alert('Invalited Action');
      this.router.navigate(['gestionar-estado']);
      return;
    }
    this.auth =  localStorage.getItem('auth');
    console.log("ID COMPONENTE EDIT" + Number(codigoEstado));
    this.estadoService.getIdEstado(codigoEstado,this.auth).subscribe(
      (data: Iestado) => { 
        this.modelEstado = data
        console.log(data);
        this.editForm.get('codigo').setValue(this.modelEstado.codigo);
        this.editForm.get('nombre').setValue(this.modelEstado.nombre);

      },
      (error) => {      
          console.error("Ha ocurrido un error, contacte con el administrador");
      }

    );
  }
  
  onSubmit() {
    this.editForm.get('fechaCreacion').setValue('2021-08-17');
    this.editForm.get('usuarioCreacion').setValue('henry.ruiz');
    this.editForm.get('fechaModificacion').setValue('2021-08-17');
    this.editForm.get('usuarioModificacion').setValue('henry.ruiz');
    this.estadoService.putEstado(this.editForm.value,this.auth).subscribe(
      data => { this.router.navigate(['gestionar-estado']); }
    );
    
  }

}
