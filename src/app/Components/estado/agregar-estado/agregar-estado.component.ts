import { Component, OnInit } from '@angular/core';
import { Iestado } from 'src/app/Interfaces/iestado';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { EstadoService } from 'src/app/Services/estado.service';

@Component({
  selector: 'app-agregar-estado',
  templateUrl: './agregar-estado.component.html',
  styleUrls: ['./agregar-estado.component.css']
})
export class AgregarEstadoComponent implements OnInit {

  modelEstado : Iestado = {} as any;
  addForm : FormGroup;
  auth: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private estadoService: EstadoService) { }

  ngOnInit() {

    this.addForm = this.formBuilder.group
    ({
      nombre: [],
      fechaCreacion: [],
      usuarioCreacion: [],
      fechaModificacion: [],
      usuarioModificacion: []
    })
    this.auth =  localStorage.getItem('auth');
  }
  
  onSubmit() {
    this.addForm.get('fechaCreacion').setValue('2021-08-17');
    this.addForm.get('usuarioCreacion').setValue('henry.ruiz');
    this.addForm.get('fechaModificacion').setValue('2021-08-17');
    this.addForm.get('usuarioModificacion').setValue('henry.ruiz');
    this.estadoService.postAddEstado(this.addForm.value,this.auth)
      .subscribe( data => {
        this.router.navigate(['gestionar-estado']);
      });
    
  }



}
