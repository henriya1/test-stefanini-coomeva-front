import { Component, OnInit } from '@angular/core';
import { ItipoIdentificacion } from 'src/app/Interfaces/itipo-identificacion';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TipoIdentService } from 'src/app/Services/tipo-ident.service';

@Component({
  selector: 'app-editar-tipo-ident',
  templateUrl: './editar-tipo-ident.component.html',
  styleUrls: ['./editar-tipo-ident.component.css']
})
export class EditarTipoIdentComponent implements OnInit {

  modelTipoIdent : ItipoIdentificacion = {} as any;
  tipoIdents: ItipoIdentificacion[] = [];
  auth: string;
  
  editForm = this.formBuilder.group(
    {
        codigo: ['', Validators.required ],
        nombre: [],
        fechaCreacion: [],
        usuarioCreacion: [],
        fechaModificacion: [],
        usuarioModificacion: []
    }
  );

  constructor(private formBuilder: FormBuilder, private router: Router, private tipoIdentService:TipoIdentService) { }

  ngOnInit() {

    this.editForm = this.formBuilder.group(
      {
        codigo: ['', Validators.required ],
        nombre: [],
        fechaCreacion: [],
        usuarioCreacion: [],
        fechaModificacion: [],
        usuarioModificacion: []
      }
    );
   
    let codigoTipoIdent =  localStorage.getItem('codigoTipoIdent');
    if (!codigoTipoIdent) {
      alert('Invalited Action');
      this.router.navigate(['gestionar-tipo-ident']);
      return;
    }
    this.auth =  localStorage.getItem('auth');
    this.tipoIdentService.getIdTipoIdentificacion(codigoTipoIdent,this.auth).subscribe(
      (data: ItipoIdentificacion) => { 
        this.modelTipoIdent = data
 
        this.editForm.setValue(this.modelTipoIdent);

      },
      (error) => {      
          console.error("Ha ocurrido un error, contacte con el administrador");
      }

    );
  }
  
  onSubmit() {
    this.tipoIdentService.putTipoIdentificacion(this.editForm.value,this.auth).subscribe(
      data => { this.router.navigate(['gestionar-tipo-ident']); }
    );
    
  }



}
