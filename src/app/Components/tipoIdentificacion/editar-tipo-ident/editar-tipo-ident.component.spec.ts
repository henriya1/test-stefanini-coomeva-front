import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarTipoIdentComponent } from './editar-tipo-ident.component';

describe('EditarTipoIdentComponent', () => {
  let component: EditarTipoIdentComponent;
  let fixture: ComponentFixture<EditarTipoIdentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarTipoIdentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarTipoIdentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
