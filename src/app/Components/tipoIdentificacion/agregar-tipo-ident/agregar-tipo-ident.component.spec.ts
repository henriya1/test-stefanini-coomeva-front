import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarTipoIdentComponent } from './agregar-tipo-ident.component';

describe('AgregarTipoIdentComponent', () => {
  let component: AgregarTipoIdentComponent;
  let fixture: ComponentFixture<AgregarTipoIdentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarTipoIdentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarTipoIdentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
