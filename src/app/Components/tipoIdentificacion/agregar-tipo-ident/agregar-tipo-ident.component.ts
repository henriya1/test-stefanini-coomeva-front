import { Component, OnInit } from '@angular/core';
import { ItipoIdentificacion } from 'src/app/Interfaces/itipo-identificacion';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TipoIdentService } from 'src/app/Services/tipo-ident.service';

@Component({
  selector: 'app-agregar-tipo-ident',
  templateUrl: './agregar-tipo-ident.component.html',
  styleUrls: ['./agregar-tipo-ident.component.css']
})
export class AgregarTipoIdentComponent implements OnInit {

  modelTipoIdent : ItipoIdentificacion = {} as any;
  addForm : FormGroup;
  auth: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private tipoIdentService: TipoIdentService) { }

  ngOnInit() {

    this.addForm = this.formBuilder.group
    ({
      codigo: [],
      nombre: [],
      fechaCreacion: [],
      usuarioCreacion: [],
      fechaModificacion: [],
      usuarioModificacion: []
    })
    this.auth =  localStorage.getItem('auth');
  }
  
  onSubmit() {
    this.addForm.get('fechaCreacion').setValue('2021-08-17');
    this.addForm.get('usuarioCreacion').setValue('henry.ruiz');
    this.addForm.get('fechaModificacion').setValue('2021-08-17');
    this.addForm.get('usuarioModificacion').setValue('henry.ruiz');
    this.tipoIdentService.postAddTipoIdentificacion(this.addForm.value,this.auth)
      .subscribe( data => {
        this.router.navigate(['gestionar-tipo-ident']);
      });
    
  }

}
