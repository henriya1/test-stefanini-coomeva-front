import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionarTipoIdentComponent } from './gestionar-tipo-ident.component';

describe('GestionarTipoIdentComponent', () => {
  let component: GestionarTipoIdentComponent;
  let fixture: ComponentFixture<GestionarTipoIdentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionarTipoIdentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionarTipoIdentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
