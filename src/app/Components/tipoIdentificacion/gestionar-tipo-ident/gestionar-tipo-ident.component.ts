import { Component, OnInit } from '@angular/core';
import { ItipoIdentificacion } from 'src/app/Interfaces/itipo-identificacion';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TipoIdentService } from 'src/app/Services/tipo-ident.service';

@Component({
  selector: 'app-gestionar-tipo-ident',
  templateUrl: './gestionar-tipo-ident.component.html',
  styleUrls: ['./gestionar-tipo-ident.component.css']
})
export class GestionarTipoIdentComponent implements OnInit {

  tipoIdents: ItipoIdentificacion[];
  modelBusqueda: ItipoIdentificacion = {} as any;
  searchForm :FormGroup;
  auth: string;

  constructor(private router:Router,
    private formBuilder: FormBuilder, private tipoIdentService:TipoIdentService) { }

  ngOnInit() {
    this.auth =  localStorage.getItem('auth');
    this.modelBusqueda = {} as any;
    this.gestionarTipoIdent(this.modelBusqueda);
    this.searchForm = this.formBuilder.group
    ({
      CODIGO: []
    })
  }

  gestionarTipoIdent(modelB){
    this.tipoIdentService.getAllTipoIdentificacion(modelB,this.auth).subscribe(
      (data: ItipoIdentificacion[]) => this.tipoIdents = data
      ,
      (error) => {      
          console.error("Ha ocurrido un error, contacte con el administrador");
      }
    );
  }

  agregarTipoIdent(): void
  {
    this.router.navigate(['agregar-tipo-ident']);
  }

  editarTipoIdent(tipoIdent: ItipoIdentificacion)
  {
    localStorage.removeItem('codigoTipoIdent');
    localStorage.setItem('codigoTipoIdent', tipoIdent.codigo.toString());
    this.router.navigate(['editar-tipo-ident']);
  }


  eliminarTipoIdent(tipoIdent: ItipoIdentificacion)
  {
    this.modelBusqueda = {} as any;
    this.modelBusqueda.codigo = tipoIdent.codigo;
    this.tipoIdentService.deleteTipoIdentificacionId(this.modelBusqueda.codigo,this.auth).subscribe(
      (data: String) => {
      alert('El registro fue eliminado con exito');
      this.modelBusqueda = {} as any;
      this.gestionarTipoIdent(this.modelBusqueda);
      }
    )
  }

  onSubmit() {

    if (this.searchForm.value.CODIGO) {
      this.tipoIdents = [];
      this.modelBusqueda = this.searchForm.value;
      this.tipoIdentService.getIdTipoIdentificacion(this.searchForm.get('CODIGO').value,this.auth).subscribe(
        (data: ItipoIdentificacion) => this.tipoIdents[0] = data
        ,
        (error) => {      
            console.error("Ha ocurrido un error, contacte con el administrador");
        }
      );
    }
    else {
      this.modelBusqueda = {} as any;
      this.gestionarTipoIdent(this.modelBusqueda);
    }
  }
}
