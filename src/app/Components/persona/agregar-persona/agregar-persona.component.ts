import { Component, OnInit } from '@angular/core';
import { Ipersona } from 'src/app/Interfaces/ipersona';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router} from '@angular/router';
import { PersonaService } from 'src/app/Services/persona.service';

@Component({
  selector: 'app-agregar-persona',
  templateUrl: './agregar-persona.component.html',
  styleUrls: ['./agregar-persona.component.css']
})
export class AgregarPersonaComponent implements OnInit {

  modelPersona : Ipersona = {} as any;
  auth: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private personaService: PersonaService) { }

  addForm = this.formBuilder.group
    ({
      codigo: ['', Validators.required ],
      nombre: [],
      apellido: [],
      fechaNacimiento: [],
      userName: [],
      password: [],
      identificacion: [],
      codiTipoIdentificacion: [],
      codigoEstado: []
    });

  ngOnInit() {

    this.addForm = this.formBuilder.group
    ({
      codigo: ['', Validators.required ],
      nombre: [],
      apellido: [],
      fechaNacimiento: [],
      userName: [],
      password: [],
      identificacion: [],
      codiTipoIdentificacion: [],
      codigoEstado: []
    })
    this.auth =  localStorage.getItem('auth');
  }
  
  onSubmit() {
    this.personaService.postAddPersona(this.addForm.value,this.auth)
      .subscribe( data => {
        this.router.navigate(['gestion-personas']);
      });
    
  }

}
