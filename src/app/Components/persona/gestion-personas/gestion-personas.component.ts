import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ipersona } from 'src/app/Interfaces/ipersona';
import { Router} from '@angular/router';
import { AgregarPersonaComponent } from '../agregar-persona/agregar-persona.component';
import { PersonaService } from 'src/app/Services/persona.service';

@Component({
  selector: 'app-gestion-personas',
  templateUrl: './gestion-personas.component.html',
  styleUrls: ['./gestion-personas.component.css']
})
export class GestionPersonasComponent implements OnInit {

  personas: Ipersona[];
  modelBusqueda: Ipersona = {} as any;
  public searchForm :FormGroup;
  mostrar: boolean;
  auth: string;

  constructor(private router:Router,
    private formBuilder: FormBuilder, private personaService:PersonaService) { }

  ngOnInit() {
    this.auth =  localStorage.getItem('auth');
    console.log("ID COMPONENTE EDIT ANTES" + this.auth);
    this.modelBusqueda  = {} as any;
    this.consultarPersonas(this.modelBusqueda);
    this.searchForm = this.formBuilder.group
    ({
      CODIGO: ['', Validators.required]
    })

  }

  consultarPersonas(modelB){
    this.personaService.getAllPersonas(modelB,this.auth).subscribe(
      (data: Ipersona[]) => this.personas = data
      ,
      (error) => {      
          console.error("Ha ocurrido un error, contacte con el administrador");
      }
    );

  }

  agregarPersona(): void
  {
    this.router.navigate(['agregar-persona']);
  }

  editarPersona(persona: Ipersona)
  {
    localStorage.removeItem('codigoPersona');
    localStorage.setItem('codigoPersona', persona.codigo.toString());
    this.router.navigate(['editar-persona']);
  }


  eliminarPersona(persona: Ipersona)
  {
    this.modelBusqueda = {} as any;
    this.modelBusqueda.codigo = persona.codigo;
    this.personaService.deletePersonaId(this.modelBusqueda.codigo,this.auth).subscribe(
      (data: String) => {
      alert('El registro fue eliminado con exito');
      this.modelBusqueda = {} as any;
      this.consultarPersonas(this.modelBusqueda);
      }
    )
  }

  onSubmit() {
    
    if (this.searchForm.value.CODIGO) {
      this.modelBusqueda = this.searchForm.value;
      this.personaService.getPersona(this.searchForm.get('CODIGO').value,this.auth).subscribe(
        (data: Ipersona[]) => this.personas = data
        ,
        (error) => {      
            console.error("Ha ocurrido un error, contacte con el administrador");
        }
      );
    }
    else {
      this.modelBusqueda = {} as any;
      this.consultarPersonas(this.modelBusqueda);
    }
  }

}
