import { Component, OnInit } from '@angular/core';
import { Ipersona } from 'src/app/Interfaces/ipersona';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PersonaService } from 'src/app/Services/persona.service';

@Component({
  selector: 'app-editar-persona',
  templateUrl: './editar-persona.component.html',
  styleUrls: ['./editar-persona.component.css']
})
export class EditarPersonaComponent implements OnInit {
  


  modelEdit: Ipersona = {} as any;
  personas: Ipersona[] = [];
  auth: string;

  editForm = this.formBuilder.group(
    {
      codigo: ['', Validators.required ],
      nombre: [],
      apellido: [],
      fechaNacimiento: [],
      userName: [],
      password: [],
      identificacion: [],
      codiTipoIdentificacion: [],
      codigoEstado: []
    }
  );
  
  constructor(private router: Router, private formBuilder: FormBuilder, private personaService: PersonaService) { }

  ngOnInit() {
    this.editForm = this.formBuilder.group(
      {
        codigo: ['', Validators.required ],
        nombre: [],
        apellido: [],
        fechaNacimiento: [],
        userName: [],
        password: [],
        identificacion: [],
        codiTipoIdentificacion: [],
        codigoEstado: []
      }
    );

    let codigoPersona =  localStorage.getItem('codigoPersona');
    console.log("ID COMPONENTE EDIT ANTES" + codigoPersona);
    if (!codigoPersona) {
      alert('Invalited Action');
      this.router.navigate(['gestionar-personas']);
      return;
    }

    console.log("ID COMPONENTE EDIT" + Number(codigoPersona));
    this.auth =  localStorage.getItem('auth');
    this.personaService.getPersona(codigoPersona,this.auth).subscribe(
      (data: Ipersona[]) => { 
        this.personas = data
        console.log(data);
        this.editForm.setValue(this.personas[0]);

      },
      (error) => {      
          console.error("Ha ocurrido un error, contacte con el administrador");
      }

    );
    
  }

  onSubmit() {
    this.personaService.updatePersona(this.editForm.value,this.auth).subscribe(
      data => { this.router.navigate(['gestion-personas']); }
    );
    
  }

}
