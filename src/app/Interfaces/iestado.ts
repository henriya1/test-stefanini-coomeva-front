export interface Iestado {
    codigo: number;
    nombre: string;
    fechaCreacion: string;
    usuarioCreacion: string;
    fechaModificacion: string;
    usuarioModificacion: string;
}
