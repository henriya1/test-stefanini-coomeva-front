export interface Ipersona {
    codigo: string;
    nombre: string;
    apellido: string;
    fechaNacimiento: string;
    userName: string;
    password: string;
    identificacion: string;
    codiTipoIdentificacion: string;
    codigoEstado: string;
}

