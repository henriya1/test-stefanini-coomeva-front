export interface ItipoIdentificacion {
    codigo: string;
    nombre: string;
    fechaCreacion: string;
    usuarioCreacion: string;
    fechaModificacion: string;
    usuarioModificacion: string;
}
