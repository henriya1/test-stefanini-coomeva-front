import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GestionPersonasComponent } from "./Components/persona/gestion-personas/gestion-personas.component";
import { AgregarPersonaComponent} from "./Components/persona/agregar-persona/agregar-persona.component";
import { EditarPersonaComponent } from "./Components/persona/editar-persona/editar-persona.component";
import { GestionarTipoIdentComponent} from "./Components/tipoIdentificacion/gestionar-tipo-ident/gestionar-tipo-ident.component";
import { AgregarTipoIdentComponent } from "./Components/tipoIdentificacion/agregar-tipo-ident/agregar-tipo-ident.component";
import { EditarTipoIdentComponent } from "./Components/tipoIdentificacion/editar-tipo-ident/editar-tipo-ident.component";
import { GestionarEstadoComponent } from "./Components/estado/gestionar-estado/gestionar-estado.component";
import { AgregarEstadoComponent } from "./Components/estado/agregar-estado/agregar-estado.component";
import { EditarEstadoComponent } from "./Components/estado/editar-estado/editar-estado.component";
import { InicioSesionComponent } from "./Components/inicioSesion/inicio-sesion/inicio-sesion.component";


const routes: Routes = [
  
  {path: '', component: InicioSesionComponent},
  {path: 'agregar-persona',component: AgregarPersonaComponent},
  {path: 'editar-persona',component: EditarPersonaComponent},
  {path: 'gestion-personas',component: GestionPersonasComponent},
  {path: 'agregar-tipo-ident',component: AgregarTipoIdentComponent},
  {path: 'editar-tipo-ident',component: EditarTipoIdentComponent},
  {path: 'gestionar-tipo-ident',component: GestionarTipoIdentComponent},
  {path: 'agregar-estado',component: AgregarEstadoComponent},
  {path: 'editar-estado',component: EditarEstadoComponent},
  {path: 'gestionar-estado',component: GestionarEstadoComponent}
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRouting {}