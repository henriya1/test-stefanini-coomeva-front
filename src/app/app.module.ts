import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRouting} from './app-routing'
import { AppComponent } from './app.component';
import { GestionPersonasComponent } from './Components/persona/gestion-personas/gestion-personas.component';
import { AgregarPersonaComponent } from './Components/persona/agregar-persona/agregar-persona.component';
import { EditarPersonaComponent } from './Components/persona/editar-persona/editar-persona.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GestionarTipoIdentComponent } from './Components/tipoIdentificacion/gestionar-tipo-ident/gestionar-tipo-ident.component';
import { AgregarTipoIdentComponent } from './Components/tipoIdentificacion/agregar-tipo-ident/agregar-tipo-ident.component';
import { EditarTipoIdentComponent } from './Components/tipoIdentificacion/editar-tipo-ident/editar-tipo-ident.component';
import { GestionarEstadoComponent } from './Components/estado/gestionar-estado/gestionar-estado.component';
import { AgregarEstadoComponent } from './Components/estado/agregar-estado/agregar-estado.component';
import { EditarEstadoComponent } from './Components/estado/editar-estado/editar-estado.component';
import { InicioSesionComponent } from './Components/inicioSesion/inicio-sesion/inicio-sesion.component';
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    GestionPersonasComponent,
    AgregarPersonaComponent,
    EditarPersonaComponent,
    GestionarTipoIdentComponent,
    AgregarTipoIdentComponent,
    EditarTipoIdentComponent,
    GestionarEstadoComponent,
    AgregarEstadoComponent,
    EditarEstadoComponent,
    InicioSesionComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
