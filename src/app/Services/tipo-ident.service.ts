import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TipoIdentService {

  public url = "";

  constructor(private http: HttpClient) { }

  getAllTipoIdentificacion(model,auth) {
    this.url = "api/getAllTipoIdentificacion";
    return this.get(
      JSON.stringify(model),auth
    );
  }

  getIdTipoIdentificacion(model,auth) {
    this.url = "api/getIdTipoIdentificacion/"+model;
    model = {} as any;
    return this.get(
      JSON.stringify(model),auth
    );
  }
  
  postAddTipoIdentificacion(model,auth) {
    this.url = "api/postAddTipoIdentificacion";
    //console.log(JSON.stringify(model));
    return this.post(
      JSON.stringify(model),auth
    );
  
  }

  putTipoIdentificacion(model,auth) {
    this.url = "api/putTipoIdentificacion";
    return this.put(
      JSON.stringify(model),auth
    );
  
  }

  deleteTipoIdentificacionId(model,auth) {
    this.url = "api/deleteTipoIdentificacionId/"+model;

    return this.delete(
      JSON.stringify(model),auth
    );

    
  
  }

  /**
   * Metodo para consultar
   * @param queryString parametros
   */
  get(queryString: any,auth:any) {
    let params: HttpParams = new HttpParams().set(
      "value",
      JSON.stringify(queryString)
    );

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",auth)

    return this.http.get(this.url, {
      params: params,
      headers: header,
    });
  }
  
  /**
   * Metodo para Crear
   * @param body contenido a guardar
   * @param queryString parametros
   */
  post(body?: any, queryString?: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",queryString)

    return this.http.post(this.url, body, {
      params: params,
      headers: header,
    });
  }

  /**
   * Metodo para actualizar
   * @param body contenido a guardar
   * @param queryString parametros
   */
  put(body?: any, queryString?: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",queryString)

    return this.http.put(this.url, body, {
      params: params,
      headers: header,
    });
  }

  /**
   * Metodo para eliminar
   * @param queryString parametros
   * @param wordKeyWs palabra clave ws
   */
  delete(queryString: any, auth:any) {
    let params: HttpParams = new HttpParams().set(
      "value",
      JSON.stringify(queryString)
    );

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",auth)

    return this.http.delete(this.url, {
      params: params,
      headers: header,
      responseType: 'text'
    });
  }
}
