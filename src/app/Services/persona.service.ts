import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  public url = "";

  constructor(private http: HttpClient) { }

  getAllPersonas(model,auth) {
    this.url = "api/getAllPersonas";
    return this.get(
      JSON.stringify(model),auth
    );
  }

  getPersona(model,auth) {
    this.url = "api/getPersona/"+model;
    model = {} as any;
    return this.get(
      JSON.stringify(model),auth
    );
  }
  
  postAddPersona(model,auth) {
    this.url = "api/postAddPersona";
    console.log(auth);
    return this.post(
      JSON.stringify(model),auth
    );
  
  }

  updatePersona(model,auth) {
    this.url = "api/updatePersona";
    return this.put(
      JSON.stringify(model),auth
    );
  
  }

  deletePersonaId(model,auth) {
    this.url = "api/deletePersonaId/"+model;

    return this.delete(
      JSON.stringify(model),auth
    );

    
  
  }

  /**
   * Metodo para consultar
   * @param queryString parametros
   */
  get(queryString: any, auth: any) {
    let params: HttpParams = new HttpParams().set(
      "value",
      JSON.stringify(queryString)
    );

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",auth)

    return this.http.get(this.url, {
      params: params,
      headers: header,
    });
  }
  
  /**
   * Metodo para Crear
   * @param body contenido a guardar
   * @param queryString parametros
   */
  post(body?: any, queryString?: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",queryString)

    return this.http.post(this.url, body, {
      params: params,
      headers: header,
    });
  }

  /**
   * Metodo para actualizar
   * @param body contenido a guardar
   * @param queryString parametros
   */
  put(body?: any, queryString?: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",queryString)

    return this.http.put(this.url, body, {
      params: params,
      headers: header,
    });
  }

  /**
   * Metodo para eliminar
   * @param queryString parametros
   * @param wordKeyWs palabra clave ws
   */
  delete(queryString: any, auth:any) {
    let params: HttpParams = new HttpParams().set(
      "value",
      JSON.stringify(queryString)
    );

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",auth)

    return this.http.delete(this.url, {
      params: params,
      headers: header,
      responseType: 'text'
    });
  }
  
}
