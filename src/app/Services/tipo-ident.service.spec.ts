import { TestBed, inject } from '@angular/core/testing';

import { TipoIdentService } from './tipo-ident.service';

describe('TipoIdentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoIdentService]
    });
  });

  it('should be created', inject([TipoIdentService], (service: TipoIdentService) => {
    expect(service).toBeTruthy();
  }));
});
