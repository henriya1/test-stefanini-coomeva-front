import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public url = "";

  constructor(private http: HttpClient) { }


  validatelogin(model) {
    this.url = "api";
    return this.postAuth(
      JSON.stringify(model)
    );
  }

  /**
   * Metodo para consultar
   * @param queryString parametros
   */
  get(queryString: any, auth: any) {
    let params: HttpParams = new HttpParams().set(
      "value",
      JSON.stringify(queryString)
    );

    let header: HttpHeaders = new HttpHeaders()
      .set("Authorization",auth)

    return this.http.get(this.url, {
      params: params,
      headers: header,
    });
  }
  
  /**
   * Metodo para Crear
   * @param body contenido a guardar
   * @param queryString parametros
   */
  post(body: any, auth: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",auth)

    return this.http.post(this.url, body, {
      params: params,
      headers: header,
    });
  }

  /**
   * Metodo para actualizar
   * @param body contenido a guardar
   * @param queryString parametros
   */
  put(body: any, auth: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",auth)

    return this.http.put(this.url, body, {
      params: params,
      headers: header,
    });
  }

  /**
   * Metodo para eliminar
   * @param queryString parametros
   * @param wordKeyWs palabra clave ws
   */
  delete(queryString: any, auth: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("Authorization",auth)

    return this.http.delete(this.url, {
      params: params,
      headers: header,
      responseType: 'text'
    });
  }

  /**
   * Metodo para Crear
   * @param body contenido a guardar
   */
  postAuth(body?: any) {
    let params: HttpParams = new HttpParams();

    let header: HttpHeaders = new HttpHeaders();

    return this.http.post(this.url, body, {
      headers: header,
      observe: 'response' as 'response'
    });
  }

}
